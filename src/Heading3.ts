import Tag from "./Tag";

class Heading3 extends Tag {
  constructor() {
    super(/^\[\*\* (.*)\]/gm, "[** $1]", /^### (.*)/gm, "### $1");
  }
}

export default Heading3;
