import Tag from "./Tag";

class Link extends Tag {
  constructor() {
    super(
      /\[(.*?) (http.*?)\]/gm,
      "[$1 $2]",
      /\[(.*?)\]\((.*?)\)/gm,
      "[$1]($2)"
    );
  }
}

export default Link;
