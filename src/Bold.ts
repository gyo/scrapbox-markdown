import Tag from "./Tag";

class Bold extends Tag {
  constructor() {
    super(/\[\[(.*?)\]\]/gm, "[[$1]]", /\*\*(.*?)\*\*/gm, "**$1**");
  }
}

export default Bold;
