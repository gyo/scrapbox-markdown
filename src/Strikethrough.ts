import Tag from "./Tag";

class Strikethrough extends Tag {
  constructor() {
    super(/\[\/ (.*?)\]/gm, "[/ $1]", /_(.*?)_/gm, "_$1_");
  }
}

export default Strikethrough;
