import Tag from "./Tag";

class Heading4 extends Tag {
  constructor() {
    super(/^\[\* (.*)\]/gm, "[* $1]", /^#### (.*)/gm, "#### $1");
  }
}

export default Heading4;
