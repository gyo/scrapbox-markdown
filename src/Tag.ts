type IReplacementCallbackType = (match: string, ...args: any[]) => string;

class Tag {
  protected sPattern: RegExp;
  protected sReplacement: string | IReplacementCallbackType;
  protected mPattern: RegExp;
  protected mReplacement: string | IReplacementCallbackType;

  constructor(
    sPattern: RegExp,
    sReplacement: string | IReplacementCallbackType,
    mPattern: RegExp,
    mReplacement: string | IReplacementCallbackType
  ) {
    this.sPattern = sPattern;
    this.sReplacement = sReplacement;
    this.mPattern = mPattern;
    this.mReplacement = mReplacement;
  }

  public sToM(text: string) {
    // https://github.com/Microsoft/TypeScript/issues/22378
    if (typeof this.mReplacement === "string") {
      return text.replace(this.sPattern, this.mReplacement);
    } else {
      return text.replace(this.sPattern, this.mReplacement);
    }
  }

  public mToS(text: string) {
    // https://github.com/Microsoft/TypeScript/issues/22378
    if (typeof this.sReplacement === "string") {
      return text.replace(this.mPattern, this.sReplacement);
    } else {
      return text.replace(this.mPattern, this.sReplacement);
    }
  }
}

export default Tag;
