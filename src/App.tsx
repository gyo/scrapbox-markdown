import * as React from "react";
import "./App.css";
import mToS from "./mToS";
import sToM from "./sToM";

interface IMessageInputEvent extends React.FormEvent<HTMLInputElement> {
  target: HTMLInputElement;
}

interface IState {
  markdownText: string;
  scrapboxText: string;
}

class App extends React.Component<{}, IState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      markdownText: "",
      scrapboxText: ""
    };

    this.handleScrapbox = this.handleScrapbox.bind(this);
    this.handleMarkdown = this.handleMarkdown.bind(this);
  }

  public handleScrapbox(e: IMessageInputEvent): void {
    const text = e.target.value;
    this.setState({
      markdownText: sToM(text),
      scrapboxText: text
    });
  }

  public handleMarkdown(e: IMessageInputEvent): void {
    const text = e.target.value;
    this.setState({
      markdownText: text,
      scrapboxText: mToS(text)
    });
  }

  public render() {
    return (
      <div className="app">
        <div className="app__input-area form">
          <div className="form-title">
            <div>Scrapbox</div>
          </div>
          <div className="form-textarea">
            <textarea
              rows={10}
              value={this.state.scrapboxText}
              onChange={this.handleScrapbox.bind(null)}
            />
          </div>
        </div>

        <div className="app__output-area form">
          <div className="form-title">
            <div>Markdown</div>
          </div>
          <div className="form-textarea">
            <textarea
              rows={10}
              value={this.state.markdownText}
              onChange={this.handleMarkdown.bind(null)}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
