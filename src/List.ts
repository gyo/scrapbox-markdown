import Tag from "./Tag";

class List extends Tag {
  constructor() {
    super(
      /^([ 　\t]+)([^ 　\t].*)/gm,
      (match: string, ...args: any[]) => {
        const p1 = args[0];
        const p2 = args[1];
        const listLevel = (p1.length - 1) / 2;
        const sPattern = Array(listLevel + 1)
          .fill(" ")
          .join("");
        return `${sPattern}${p2}`;
      },
      /^( *-) (.*)/gm,
      (match: string, ...args: any[]) => {
        const p1 = args[0];
        const p2 = args[1];
        const listLevel = p1.length;
        const mPattern =
          Array(listLevel - 1)
            .fill("  ")
            .join("") + "- ";
        return `${mPattern}${p2}`;
      }
    );
  }
}

export default List;
