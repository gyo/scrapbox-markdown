import Bold from "./Bold";
import Heading2 from "./Heading2";
import Heading3 from "./Heading3";
import Heading4 from "./Heading4";
import Link from "./Link";
import List from "./List";
import Strikethrough from "./Strikethrough";

const bold = new Bold();
const heading2 = new Heading2();
const heading3 = new Heading3();
const heading4 = new Heading4();
const link = new Link();
const list = new List();
const strikethrough = new Strikethrough();

const mToS = (text: string): string => {
  return heading2.mToS(
    heading3.mToS(
      heading4.mToS(bold.mToS(link.mToS(list.mToS(strikethrough.mToS(text)))))
    )
  );
};

export default mToS;
